<?php

/*
* Function for echoing out breadcrumbs
*
*/
function echo_breadcrumbs() {
    echo '<a href="'.home_url().'" rel="nofollow">Home</a>';
    if (is_category() || is_single()) {
        echo "<span class=“breadcrumb_sep”>&#187</span>";
        the_category(' &bull; ');
        if (is_single()) {
            echo " <span class=“breadcrumb_sep”>&#187</span>";
            the_title();
        }
    } elseif (is_page()) {
        $currentid = $post->ID;
        $currentParentId = wp_get_post_parent_id( $current );


        while($currentParentId != 0 ) {
            $nextParent = wp_get_post_parent_id($currentParentId);
            echo "<span class=“breadcrumb_sep”>&#187</span>";
            echo '<a href="'. get_permalink($currentParentId) .'" rel="nofollow">' . get_the_title($currentParentId) . '</a>';
            $currentParentId = $nextParent;
        }

        echo "<span class=“breadcrumb_sep”>&#187</span>";
        echo the_title();
    } elseif (is_search()) {
        echo "<span class=“breadcrumb_sep”>&#187</span>Search Results for... ";
        echo '"<em>';
        echo the_search_query();
        echo '</em>"';
    }
}

/*
* Return the breadcrumbs instead of printing them out
*/
function get_breadcrumbs() {
    $bread_crumbs = '';
    $bread_crumbs .= '<a href="'.home_url().'" rel="nofollow">Home</a>';
    if (is_category() || is_single()) {
        $bread_crumbs .= "<span class=“breadcrumb_sep”>&#187</span>";
        the_category(' &bull; ');
        if (is_single()) {
            $bread_crumbs .= "<span class=“breadcrumb_sep”>&#187</span>";
            $bread_crumbs .= get_the_title();
        }
    } elseif (is_page()) {
        $currentid = $post->ID;
        $currentParentId = wp_get_post_parent_id( $current );


        while($currentParentId != 0 ) {
            $nextParent = wp_get_post_parent_id($currentParentId);
            $bread_crumbs .= "<span class=“breadcrumb_sep”>&#187</span>";
            $bread_crumbs .= '<a href="'. get_permalink($currentParentId) .'" rel="nofollow">' . get_the_title($currentParentId) . '</a>';
            $currentParentId = $nextParent;
        }

        $bread_crumbs .= "<span class=“breadcrumb_sep”>&#187</span>";
        $bread_crumbs .= get_the_title();
    } elseif (is_search()) {
        $bread_crumbs .= "<span class=“breadcrumb_sep”>&#187</span>Search Results for... ";
        $bread_crumbs .= '"<em>';
        $bread_crumbs .= get_search_query();
        $bread_crumbs .= '</em>"';
    }
    return $bread_crumbs;
}
